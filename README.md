# My Terraform Notes
# ==============================
# 02h20m_LYNDA_Learning Terraform 2019
## Terraform Installation - Ubuntu
source: https://www.terraform.io/docs/cli/install/apt.html
```shell
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -

sudo apt-add-repository "deb [arch=$(dpkg --print-architecture)] https://apt.releases.hashicorp.com $(lsb_release -cs) main"

sudo apt install terraform --yes
```
## Terraform Init
1. Create a connection file:

![TerraformInit](images/TerraformInit.png)

2. Initialize Terraform Connection
```shell
terraform init
```

## Virtual Network Example
![VirtualNetworkExample](images/VirtualNetworkExample.png)
```shell
# Show changes required by the current configuration
terraform plan

# Create or update infrastructure
terraform apply
```
### Subnet Example
![AddSubnetToAWS](images/AddSubnetToAWS.png)
### Azure VNet and Subnets
![CreateAzureConnection](images/CreateAzureConnection.png)
```shell
az login

az acount list

az account set --subscription="AZ_ACCOUNT_LIST_ID"

az ad sp create-for-rbac --role="contributor"  --scopes="/subscriptions/AZ_ACCOUNT_LIST_ID"
```
* SUBCRIPTION_ID = AZ_ACCOUNT_LIST_ID
* CLIENT_ID = APPID
* CLIENT_SECRET = PASSWORD
* TENANT_ID = TENANT

![CreateAzureVNetAndSubnets](images/CreateAzureVNetAndSubnets.png)

# ==============================
# A_01_03h30m_PLURALSIGHT_Terraform - Getting Started 2019
## Terraform Components/Architecture
![TerraformComponents](images/TerraformComponents.png)

Q: Why do you use Terraform variables?  
A: To NOT have our Cloud providers credentials hardcoded in our Terraform files.

Q: What is the term for cloud providers like AWS, Azure and GCP?  
A: They are "provider"

Q: Why do you use Terraform data source?  
A: To request

Q: What is the term for cloud components like VM, VNet, etc?  
A: They are "resource"

Q: When do you use "terraform init"?  
A: Is to let Terraform download any missing plugins to your local computer.

Q: How do you pass the .tf file that you want to run?  
A: terraform apply "MY_TERRAFORM_FILE.tf"

Q: What does "terraform.tfstate" use for?  
A: Terraform need that file to compare our script with our actual infrastructure.

Q: How can I revert the creation of my Terraform file?  
A: terraform destroy
